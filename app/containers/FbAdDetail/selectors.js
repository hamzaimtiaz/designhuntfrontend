import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectFbPage = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.fbPage,
  );
