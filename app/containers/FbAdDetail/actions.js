import {
  FETCH_FB_PAGE,
  FETCH_FB_PAGE_SUCCESS,
  FETCH_FB_PAGE_FAILED,
  TRACK_COMPANY,
  TRACK_COMPANY_SUCCESS,
  TRACK_COMPANY_FAILED
} from './constants';

export function fetchFbPage() {
  return {
    type: FETCH_FB_PAGE,
  };
}
export function FbPageLoaded(payload) {
  return {
    type: FETCH_FB_PAGE_SUCCESS,
    payload,
  };
}
export function FbPageFailed(error) {
  return {
    type: FETCH_FB_PAGE_FAILED,
    error,
  };
}

export function trackCompany(track) {
  return {
    type: TRACK_COMPANY,
    track,
  };
}


export function companyTracked(payload) {
  return {
    type: TRACK_COMPANY_SUCCESS,
    payload
  };
}
export function companyTrackFailed(error) {
  return {
    type: TRACK_COMPANY_FAILED,
    error,
  };
}

