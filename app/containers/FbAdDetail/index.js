
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import FbPageBody from 'components/fbPageBody'
import Header from 'components/header'
import Footer from 'components/footer'

import { makeSelectFbPage } from './selectors';
import { fetchFbPage, trackCompany} from './actions';



const key = 'forms';
// eslint-disable-next-line react/prop-types
function fbAdDetail(
  { dispatchLoadFbPage,
    dispatchTrackCompany,
    FbPage,
  }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadFbPage();
  }, []);

  const handleOntrack = track => {
    dispatchTrackCompany(track)
    dispatchLoadFbPage();
  };

  return (
    <div className="homeStyle">
      {/* <div>
        <Header />
      </div> */}
      <div>
      <FbPageBody
          data={FbPage}
          track={handleOntrack}
        />
      </div>
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  FbPage: makeSelectFbPage(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadFbPage: () => dispatch(fetchFbPage()),
  dispatchTrackCompany: track => dispatch(trackCompany(track)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(fbAdDetail);
