import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL,FB_PAGE_API,ACCESS_TOKEN,TRACK } from '../../assets/config';
import request from '../../utils/request';
import {
  FETCH_FB_PAGE,
  FETCH_FB_PAGE_FAILED, 
  FETCH_FB_PAGE_SUCCESS,
TRACK_COMPANY,} from './constants';

import {
  FbPageLoaded,
  FbPageFailed,
  companyTrackFailed,
  companyTracked,
} from './actions';

import jwt_decode from 'jwt-decode'

export function* fetchFbPage() {
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${FB_PAGE_API}${location.pathname.split("/")[2]}/`;
  const fbPageGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const fbPageResponse = yield call(request, requestURL,fbPageGet);
    console.log(fbPageResponse);
    /* Code for Question API call */
    yield put(FbPageLoaded(fbPageResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(FbPageFailed(err));
  }
}

export function* trackCompany(track) {
  

  var userID = jwt_decode(JSON.parse(localStorage.getItem('authentication')).access).user_id
  var payload={
    user:userID,
    company:track.track.data.company.id
  }
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
    const requestURL = `${API_URL}${TRACK}`;
    console.log("requestURL",requestURL);
  try {
    const trackResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
      }),
      body: JSON.stringify(payload),
    });
    console.log("trackResponse",trackResponse);
    yield put(companyTracked(trackResponse));
    // location.pathname = '/company';
  } catch (err) {
      console.log(err,err.response);
      yield put(companyTrackFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_FB_PAGE, fetchFbPage);
  yield takeLatest(TRACK_COMPANY, trackCompany);
}
