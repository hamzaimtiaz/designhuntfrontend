import produce from 'immer';
import { FETCH_FB_PAGE_SUCCESS, } from './constants';

export const initialState = {
  fbPage: [],
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_FB_PAGE_SUCCESS:
        draft.fbPage = action.payload;
        break;
    }
  });

export default homeReducer;
