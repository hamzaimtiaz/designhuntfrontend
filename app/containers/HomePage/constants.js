
export const FETCH_COMPANY = 'FETCH_COMPANY';
export const FETCH_COMPANY_SUCCESS = 'FETCH_COMPANY_SUCCESS';
export const FETCH_COMPANY_FAILED = 'FETCH_COMPANY_FAILED';

export const FETCH_COMPANY_COUNT = 'FETCH_COMPANY_COUNT';
export const FETCH_COMPANY_COUNT_SUCCESS = 'FETCH_COMPANY_COUNT_SUCCESS';
export const FETCH_COMPANY_COUNT_FAILED = 'FETCH_COMPANY_COUNT_FAILED';

export const FETCH_LATEST = 'FETCH_LATEST';
export const FETCH_LATEST_SUCCESS = 'FETCH_LATEST_SUCCESS';
export const FETCH_LATEST_FAILED = 'FETCH_LATEST_FAILED';

export const UNTRACK_COMPANY = 'UNTRACK_COMPANY';

