import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL, TRACK,MAIN_API1 ,MAIN_API} from '../../assets/config';
import request from '../../utils/request';

import { 
  FETCH_COMPANY, UNTRACK_COMPANY, FETCH_COMPANY_COUNT, FETCH_LATEST} from './constants';

import {
  companyLoaded,
  companyFailed,
  fetchCompany,
  companyCountLoaded,
  companyCountFailed,
  latestLoaded,
  latestFailed,
} from './actions';



export function* fetchCompanyData(data) {
  
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  // const requestURL = `${API_URL}${MAIN_API}`;
  // console.log("requestURL",data.requestURL)

  const companyGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyResponse = yield call(request, data.requestURL,companyGet);
    yield put(companyLoaded(companyResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(companyFailed(err));
  }
}

export function* fetchCompanyCountData() {
  
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${TRACK}`;
  // console.log("requestURLCOUNT",requestURL)
  const companyGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyCountResponse = yield call(request, requestURL,companyGet);
    yield put(companyCountLoaded(companyCountResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(companyCountFailed(err));
  }
}

export function* fetchLatestData() {
  
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${MAIN_API1}`;
  // console.log("requestURLCOUNT",requestURL)
  const companyGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyLatestResponse = yield call(request, requestURL,companyGet);
    yield put(latestLoaded(companyLatestResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(latestFailed(err));
  }
}

export function* untrackCompany(data) {
  
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  var requestURL = `${API_URL}${TRACK}${data.payload.tracked.id}`;
  console.log(requestURL)
  const companyGet = {
        method: 'DELETE',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyResponse = yield call(request, requestURL,companyGet);
    var requestURL = `${API_URL}${MAIN_API}`;
    // console.log('companyResponse'companyResponse)
    yield put(fetchCompany(requestURL))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(companyFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_COMPANY, fetchCompanyData);
  yield takeLatest(FETCH_LATEST, fetchLatestData);
  yield takeLatest(UNTRACK_COMPANY, untrackCompany);
  yield takeLatest(FETCH_COMPANY_COUNT, fetchCompanyCountData);
}
