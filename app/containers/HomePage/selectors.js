import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectCompany = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.company,
  );

export const makeSelectCompanyCount = () =>
createSelector(
  makeSelectDesign,
  forms => forms.company_count.count,
);

export const makeSelectLatest = () =>
createSelector(
  makeSelectDesign,
  forms => forms.latest,
);


// export const makeSelectUsers = () =>
//   createSelector(
//     makeSelectMystery,
//     forms => forms.users,
//   );
