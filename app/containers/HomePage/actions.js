import {
  FETCH_COMPANY,
  FETCH_COMPANY_SUCCESS,
  FETCH_COMPANY_FAILED,
  FETCH_COMPANY_COUNT,
  FETCH_COMPANY_COUNT_SUCCESS,
  FETCH_COMPANY_COUNT_FAILED,
  FETCH_LATEST,
  FETCH_LATEST_SUCCESS,
  FETCH_LATEST_FAILED,
  UNTRACK_COMPANY
} from './constants';

export function fetchCompany(requestURL) {
  return {
    type: FETCH_COMPANY,
    requestURL
  };
}
export function companyLoaded(payload) {
  return {
    type: FETCH_COMPANY_SUCCESS,
    payload,
  };
}
export function companyFailed(error) {
  return {
    type: FETCH_COMPANY_FAILED,
    error,
  };
}

export function fetchLatest(requestURL) {
  return {
    type: FETCH_LATEST,
    requestURL
  };
}
export function latestLoaded(payload) {
  return {
    type: FETCH_LATEST_SUCCESS,
    payload,
  };
}
export function latestFailed(error) {
  return {
    type: FETCH_LATEST_FAILED,
    error,
  };
}

export function fetchCompanyCount() {
  return {
    type: FETCH_COMPANY_COUNT,
  };
}
export function companyCountLoaded(payload) {
  return {
    type: FETCH_COMPANY_COUNT_SUCCESS,
    payload,
  };
}
export function companyCountFailed(error) {
  return {
    type: FETCH_COMPANY_COUNT_FAILED,
    error,
  };
}

export function untrackCompany(payload) {
  return {
    type: UNTRACK_COMPANY,
    payload,
  };
}

