
// import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import MainBody from 'components/mainPage'
import Header from 'components/header'
import Footer from 'components/footer'
// import QuestionsForm from 'components/questionsForm';

import { makeSelectCompany, makeSelectCompanyCount, makeSelectLatest } from './selectors';
import { fetchCompany, untrackCompany,fetchCompanyCount, fetchLatest} from './actions';

import { API_URL,MAIN_API } from '../../assets/config';


const startfilter = '';
const key = 'forms';


// eslint-disable-next-line react/prop-types
function homePage(
  { dispatchLoadCompany,
    dispatchUntrackCompany,
    dispatchTrackCompanyCount,
    dispatchLoadLatest,
    company,
    latest,
    company_count,
  }
  
  ) {

    
    const requestURL = `${API_URL}${MAIN_API}`; 

    const header_main_class = "tablink__companylist w-inline-block w-tab-link";
    const inner_main_class = "tabpane w-tab-pane";
    
    const [tab, setTab] = useState({
      header_arr:[["tablink__companylist w-inline-block w-tab-link w--current",header_main_class,header_main_class,header_main_class,header_main_class]],
      inner_arr:[["tabpane w-tab-pane w--tab-active ",inner_main_class,inner_main_class,inner_main_class,inner_main_class]],
  
    });
  
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
  dispatchLoadCompany(requestURL);
  dispatchTrackCompanyCount();
  dispatchLoadLatest();
  }, []);

  const handleOnUntrack = untrack => {
    dispatchUntrackCompany(untrack)
    dispatchLoadCompany(requestURL)
  };

  var handleTabSubmit = (ev) => {
   
    var header_arr1=[header_main_class,header_main_class,header_main_class,header_main_class,header_main_class];
    var inner_arr1=[inner_main_class,inner_main_class,inner_main_class,inner_main_class,inner_main_class];
    header_arr1[ev]= "tablink__companylist w-inline-block w-tab-link w--current "
    inner_arr1[ev]="tabpane w-tab-pane w--tab-active "
    
    setTab({...tab,header_arr:[header_arr1],inner_arr:[inner_arr1]})
    dispatchLoadCompany(requestURL)
   }
  
   var handleForwardBackwardSubmit = (ev) => {
     console.log("handleForwardBackwardSubmit",ev)
    dispatchLoadCompany(ev)
   }

  //  var handlebackwardSubmit = (ev) => {
  //   dispatchLoadCompany(requestURL)
  //  }

  return (
    <div className="homeStyle">
      {/* <Header 
      search = {search}
      setSearch = {setSearch}
      onButtonSubmit = {handleOnSubmit}/> */}
      <div>
      <MainBody
          data={company.results}
          next_prev_handle={handleForwardBackwardSubmit}
          latest_data={latest}
          next_url = {company.next}
          prev_url = {company.previous}
          handleSubmit={handleTabSubmit}
          company_count = {company_count}
          tabState={tab}
          untrack={handleOnUntrack}
        />
      </div>
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  company: makeSelectCompany(),
  company_count:makeSelectCompanyCount(),
  latest:makeSelectLatest()
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadCompany: requestURL => dispatch(fetchCompany(requestURL)),
  dispatchLoadLatest: () => dispatch(fetchLatest()),
  dispatchUntrackCompany: untrack => dispatch(untrackCompany(untrack)),
  dispatchTrackCompanyCount: () => dispatch(fetchCompanyCount()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(homePage);
