import produce from 'immer';
import { FETCH_COMPANY_SUCCESS,FETCH_COMPANY_COUNT_SUCCESS,FETCH_LATEST_SUCCESS } from './constants';

export const initialState = {
  company: [],
  company_count: [],
  latest:[],
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      

      case FETCH_COMPANY_SUCCESS:
        draft.company = action.payload;
        break;

      case FETCH_COMPANY_COUNT_SUCCESS:
        draft.company_count = action.payload;
        break;
      
      case FETCH_LATEST_SUCCESS:
      draft.latest = action.payload;
      break;
      
    }
  });

export default homeReducer;
