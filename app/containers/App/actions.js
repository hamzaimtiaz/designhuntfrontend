import {
  FETCH_USER_TOKEN,
  FETCH_USER_SUCCESS,
  FETCH_REFRESH_TOKEN,
  CLEAR_STORAGE,
  FETCH_COMPANY,
  FETCH_COMPANY_SUCCESS,
  FETCH_COMPANY_FAILED,
  LOGOUT,
} from './constants';

export function fetchUserToken() {
  return {
    type: FETCH_USER_TOKEN,
  };
}

export function userLoaded(payload) {
  return {
    type: FETCH_USER_SUCCESS,
    payload,
  };
}

export function clearSession() {
  return {
    type: CLEAR_STORAGE,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function refreshToken() {
  return {
    type: FETCH_REFRESH_TOKEN,
  };
}