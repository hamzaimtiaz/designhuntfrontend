import {
  FETCH_EMAIL,
  FETCH_EMAIL_SUCCESS,
  FETCH_EMAIL_FAILED,
  LOADING,
  TRACK_COMPANY,
  TRACK_COMPANY_SUCCESS,
  TRACK_COMPANY_FAILED
} from './constants';

// export function makeForm(formValue,questions) {
//   return {
//     type: MAKE_FORM,
//     formValue,questions,
//   };
// }
// export function formLoaded(payload) {
//   return {
//     type: MAKE_FORM_SUCCESS,
//     payload,
//   };
// }
// export function formFailed(error) {
//   return {
//     type: MAKE_FORM_FAILED,
//     error,
//   };
// }

export function fetchEmail() {
  return {
    type: FETCH_EMAIL,
  };
}
export function emailLoaded(payload) {
  return {
    type: FETCH_EMAIL_SUCCESS,
    payload,
  };
}
export function emailFailed(error) {
  return {
    type: FETCH_EMAIL_FAILED,
    error,
  };
}

// export function fetchUserTitles(param) {
//   console.log('In actions')
//   return {
//     type: FETCH_USER_TITLE,
//     param,
//   };
// }
// export function userLoaded(payload) {
//   return {
//     type: FETCH_USER_SUCCESS,
//     payload,
//   };
// }
// export function userFailed(error) {
//   return {
//     type: FETCH_USER_FAILED,
//     error,
//   };
// }

export function trackCompany(track) {
  return {
    type: TRACK_COMPANY,
    track,
  };
}


export function companyTracked(payload) {
  return {
    type: TRACK_COMPANY_SUCCESS,
    payload
  };
}
export function companyTrackFailed(error) {
  return {
    type: TRACK_COMPANY_FAILED,
    error,
  };
}
