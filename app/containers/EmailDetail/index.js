
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import EmailBody from 'components/emailBody'
import Header from 'components/header'
import Footer from 'components/footer'

import { makeSelectEmails } from './selectors';
import { fetchEmail,trackCompany} from './actions';


const key = 'forms';



// eslint-disable-next-line react/prop-types
function emailDetail(
  { dispatchLoadEmails,
    dispatchTrackCompany,
    emails,
  }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadEmails();
  }, []);

  const handleOntrack = track => {
    dispatchTrackCompany(track)
    dispatchLoadEmails();
  };

  return (
    <div className="homeStyle">
      {/* <div>
        <Header />
      </div> */}
      <div>
      <EmailBody
          data={emails}
          track={handleOntrack}
        />
      </div>
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  emails: makeSelectEmails(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadEmails: () => dispatch(fetchEmail()),
  dispatchTrackCompany: track => dispatch(trackCompany(track)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(emailDetail);
