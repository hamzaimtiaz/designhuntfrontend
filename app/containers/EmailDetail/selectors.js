import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectEmails = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.emails,
  );


// export const makeSelectUsers = () =>
//   createSelector(
//     makeSelectMystery,
//     forms => forms.users,
//   );
