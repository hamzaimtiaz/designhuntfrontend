import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL,EMAIL_API,ACCESS_TOKEN,TRACK } from '../../assets/config';
import request from '../../utils/request';
import {
  FETCH_EMAIL_FAILED,
  FETCH_EMAIL_SUCCESS, 
  FETCH_EMAIL,
TRACK_COMPANY} from './constants';

import jwt_decode from 'jwt-decode'

import {
  emailLoaded,
  emailFailed,
  companyTrackFailed,
  companyTracked,
} from './actions';
import { Token } from 'react-bootstrap-typeahead';

export function* fetchEmail() {
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${EMAIL_API}${location.pathname.split("/")[2]}/`;
  console.log(requestURL)
  const emailGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const emailResponse = yield call(request, requestURL,emailGet);
    /* Code for Question API call */
    yield put(emailLoaded(emailResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(emailFailed(err));
  }
}

export function* trackCompany(track) {
  

  var userID = jwt_decode(JSON.parse(localStorage.getItem('authentication')).access).user_id
  var payload={
    user:userID,
    company:track.track.data.company.id
  }
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
    const requestURL = `${API_URL}${TRACK}`;
    console.log("requestURL",requestURL);
  try {
    const trackResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
      }),
      body: JSON.stringify(payload),
    });
    console.log("trackResponse",trackResponse);
    yield put(companyTracked(trackResponse));
    // location.pathname = '/company';
  } catch (err) {
      console.log(err,err.response);
      yield put(companyTrackFailed(err));
  }
}

export default function* inventorySaga() {
 
  yield takeLatest(FETCH_EMAIL, fetchEmail);
  yield takeLatest(TRACK_COMPANY, trackCompany);
}
