import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { fetchToken } from './actions';
import {makeSelectLogin} from '../App/selectors';
import { select } from 'redux-saga/effects';

function Login({dispatchLoadToken,login_error}) {
  
  const [login, setLogin] = useState({
    "username":"",
    "password":""
  });
  
  useEffect(() => {
    localStorage.clear();
    }, []);

  const handleOnChange = ev => {
    setLogin({ ...login, [ev.target.name]: ev.target.value });
  };

  const handleOnSubmit = () => {
    
    console.log("Button clicked")
    dispatchLoadToken(login);
    
  };

  return (
    <div id="login">
      <div class="section sign-up">
    <div>
      <div class="w-layout-grid grid-7">
        <div id="w-node-f3d56d634cb7-edcf6928" class="left-grid">
          <div class="div-block-39">
            <div class="div-block-40"><img src={require("../images/Designhunt-Logo.svg")} alt=""/></div>
            <div class="div-block-42">
              <h1 class="h3 white"><strong class="bold-text-8">Monitor competitors emails, facebook ads and website changes in one place. </strong></h1>
            </div>
            <div class="div-block-41"><img src={require("../images/Group-87.svg")} alt="" class="image-12"/></div>
          </div>
        </div>


        <div class="right-grid">
          <div class="paragraph sign-in">New to designHunt? 
          
          <a href={`signup/`}><span><strong class="bold-text-7">Sign up</strong></span></a>
          </div>
          <div class="div-block-43">
            <div>
              <h1 class="h2"><strong>Log in</strong></h1>
              <div class="paragraph">Welcome back, log in to see the latest data!</div>
            </div>

            {login_error.response &&
              <div>
                
              <div class="paragraph" ><p style={{ color: 'red' }}>Email or Password is incorrect!</p></div>
            </div>
            }
            

           
            <div class="w-form" >
              <form id="wf-form-Sign-Up-Form" name="wf-form-Sign-Up-Form" data-name="Sign Up Form" class="form" >
                <div><label for="Email" class="field-label">Email Address</label>
                
                <input type="email" maxlength="256" name="username" data-name="Email" id="Email" required="" class="text-field w-input" onChange={handleOnChange} value={login.username}/></div>
                <div><label for="Password" class="field-label">Password</label>

                <input type="password" class="text-field w-input" maxlength="256" name="password" data-name="Password" id="Password" required="" onChange={handleOnChange} value={login.password}/></div>
                
                <input type="button" name="submit" value="Login" data-wait="Please wait..."  class="bluebutton sign-up w-button" style = {{cursor:'pointer'}} onClick={handleOnSubmit}
                  />
                
                
                <div class="text-block-18">Forgot Passport</div>
              </form>
              <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
              </div>
              <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




      
    </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  login_error: makeSelectLogin(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadToken: login => dispatch(fetchToken(login)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(Login);
