import {
  FETCH_TOKEN,
  FETCH_TOKEN_FAILED,
  FETCH_TOKEN_SUCCESS,
} from './constants';

export function fetchToken(login) {
  return {
    type: FETCH_TOKEN,
    login,
  };
}

export function tokenLoaded(payload) {
  
  return {
    type: FETCH_TOKEN_SUCCESS,
    payload,
  };
}
export function tokenFailed(error) {
  return {
    type: FETCH_TOKEN_FAILED,
    error,
  };
}

