import { call, put, takeLatest } from 'redux-saga/effects';
import { TOKEN_API, API_URL } from '../../assets/config';
import request from '../../utils/request';
import {FETCH_TOKEN,} from './constants'
import { tokenFailed, tokenLoaded } from './actions';

export function* getToken(login) {

  console.log("Login Credentials",JSON.stringify(login.login))
  const requestURL = `${API_URL}${TOKEN_API}`;
    
    try {
    const loginResponse = yield call(request, requestURL, {
      // method: 'POST',
      // headers: new Headers({
      //   'Content-Type': 'application/json',
      // }),
      // body: JSON.stringify(login.login),
    });
    yield put(tokenLoaded(loginResponse));
    // localStorage.setItem('authentication', JSON.stringify(loginResponse));
    console.log("we are here")

  } catch (err) {
    
    yield put(tokenFailed(err));
  }
}

// export default function* inventorySaga() {
//   console.log("we are here bullshti")
//   yield takeLatest(FETCH_TOKEN, getToken);
// }

