import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL,COMPANY_DETAIL_API,TRACK } from '../../assets/config';
import request from '../../utils/request';
import {
  
  FETCH_COMPANY,
TRACK_COMPANY} from './constants';

import {
  companyLoaded,
  companyFailed,
  companyTrackFailed,
  companyTracked,
} from './actions';

import jwt_decode from 'jwt-decode'


export function* fetchCompanyDetail() {
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${COMPANY_DETAIL_API}${location.pathname.split("/")[2]}/`;
  console.log('requestURL',requestURL)
  const companyDetailGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyDetailResponse = yield call(request, requestURL,companyDetailGet);
    /* Code for Question API call */
    yield put(companyLoaded(companyDetailResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(companyFailed(err));
  }
}

export function* trackCompany(track) {
  
  console.log("Track",location.pathname.split("/")[2])
  var userID = jwt_decode(JSON.parse(localStorage.getItem('authentication')).access).user_id
  var payload={
    user:userID,
    company:track.track.data.id
  }
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
    const requestURL = `${API_URL}${TRACK}`;
    console.log("requestURL",requestURL);
  try {
    const trackResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
      }),
      body: JSON.stringify(payload),
    });
    console.log("trackResponse",trackResponse);
    yield put(companyTracked(trackResponse));
    // location.pathname = '/company';
  } catch (err) {
      console.log(err,err.response);
      yield put(companyTrackFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_COMPANY, fetchCompanyDetail);
  yield takeLatest(TRACK_COMPANY, trackCompany);
}
