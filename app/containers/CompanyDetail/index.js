
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import CompanyBody from 'components/companyDetail'
import Header from 'components/header'
import Footer from 'components/footer'
// import QuestionsForm from 'components/questionsForm';

import { makeSelectCompanyDetails } from './selectors';
import { fetchCompany,trackCompany} from './actions';


const startfilter = '';
const key = 'forms';



// eslint-disable-next-line react/prop-types
function companyDetail(
  { dispatchLoadCompany,
    dispatchTrackCompany,
    company,
  }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadCompany();
  }, []);

  const handleOntrack = track => {
    dispatchTrackCompany(track)
    dispatchLoadCompany();
  };

  return (
    <div className="homeStyle">
      
      <div>
      <CompanyBody
          data={company}
          track={handleOntrack}
        />
      </div>
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  company: makeSelectCompanyDetails(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadCompany: () => dispatch(fetchCompany()),
  dispatchTrackCompany: track => dispatch(trackCompany(track)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(companyDetail);
