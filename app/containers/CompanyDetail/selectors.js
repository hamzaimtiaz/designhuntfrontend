import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectCompanyDetails = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.company_details,
  );
