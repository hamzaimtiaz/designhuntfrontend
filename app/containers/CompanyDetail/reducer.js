import produce from 'immer';
import { FETCH_COMPANY_SUCCESS, } from './constants';

export const initialState = {
  company_details: [],
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      
      case FETCH_COMPANY_SUCCESS:
        draft.company_details = action.payload;
        break;
    }
  });

export default homeReducer;
