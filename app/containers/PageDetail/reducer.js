import produce from 'immer';
import { FETCH_PAGE_SUCCESS, } from './constants';

export const initialState = {
  Page: [],
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PAGE_SUCCESS:
        draft.Page = action.payload;
        break;
    }
  });

export default homeReducer;
