import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL,PAGE_API,ACCESS_TOKEN,TRACK } from '../../assets/config';
import request from '../../utils/request';
import {
  FETCH_PAGE,
  TRACK_COMPANY,
} from './constants';

import {
  PageLoaded,
  PageFailed,
  companyTrackFailed,
  companyTracked,
} from './actions';

import jwt_decode from 'jwt-decode'


export function* fetchPage() {
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  const requestURL = `${API_URL}${PAGE_API}${location.pathname.split("/")[2]}/`;
  const PageGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const PageResponse = yield call(request, requestURL,PageGet);
    console.log(PageResponse);
    /* Code for Question API call */
    yield put(PageLoaded(PageResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(PageFailed(err));
  }
}

export function* trackCompany(track) {
  

  var userID = jwt_decode(JSON.parse(localStorage.getItem('authentication')).access).user_id
  var payload={
    user:userID,
    company:track.track.data.company.id
  }
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
    const requestURL = `${API_URL}${TRACK}`;
    console.log("requestURL",requestURL);
  try {
    const trackResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
      }),
      body: JSON.stringify(payload),
    });
    console.log("trackResponse",trackResponse);
    yield put(companyTracked(trackResponse));
    // location.pathname = '/company';
  } catch (err) {
      console.log(err,err.response);
      yield put(companyTrackFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_PAGE, fetchPage);
  yield takeLatest(TRACK_COMPANY, trackCompany);
}
