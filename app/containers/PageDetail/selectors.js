import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectPage = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.Page,
  );

