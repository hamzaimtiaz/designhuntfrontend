import {
  FETCH_PAGE,
  FETCH_PAGE_SUCCESS,
  FETCH_PAGE_FAILED,
  TRACK_COMPANY,
  TRACK_COMPANY_SUCCESS,
  TRACK_COMPANY_FAILED
} from './constants';

export function fetchPage() {
  return {
    type: FETCH_PAGE,
  };
}
export function PageLoaded(payload) {
  return {
    type: FETCH_PAGE_SUCCESS,
    payload,
  };
}

export function PageFailed(error) {
  return {
    type: FETCH_PAGE_FAILED,
    error,
  };
}
export function trackCompany(track) {
  return {
    type: TRACK_COMPANY,
    track,
  };
}


export function companyTracked(payload) {
  return {
    type: TRACK_COMPANY_SUCCESS,
    payload
  };
}
export function companyTrackFailed(error) {
  return {
    type: TRACK_COMPANY_FAILED,
    error,
  };
}
