
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import PageBody from 'components/pageBody'
import Header from 'components/header'
import Footer from 'components/footer'
// import QuestionsForm from 'components/questionsForm';

import { makeSelectPage } from './selectors';
import { fetchPage, trackCompany} from './actions';


const startfilter = '';
const key = 'forms';
const questions_form ={
  formName:``,
  user_id:0,
  date: new Date(),
  startTime: new Date().getTime(),
  endTime: new Date().getTime(),
  Questions: []
}


// eslint-disable-next-line react/prop-types
function pageDetail(
  { dispatchLoadPage,
    dispatchTrackCompany,
    Page,
  }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadPage();
  }, []);

  const handleOntrack = track => {
    dispatchTrackCompany(track)
    dispatchLoadPage();
  };

  return (
    <div className="homeStyle">
      {/* <div>
        <Header />
      </div> */}
      <div>
      <PageBody
          data={Page}
          track={handleOntrack}
        />
      </div>
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  Page: makeSelectPage(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadPage: () => dispatch(fetchPage()),
  dispatchTrackCompany: track => dispatch(trackCompany(track)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(pageDetail);
