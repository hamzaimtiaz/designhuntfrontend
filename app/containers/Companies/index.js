
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import CompanyBody from 'components/company'
import Header from 'components/header'
import Footer from 'components/footer'

import { fetchCompany, fetchCompanySearch, trackCompany} from './actions';
import {makeSelectCompany} from './selectors';
import { API_URL,COMPANY_API, SEARCH_PARAM } from '../../assets/config';


const startfilter = '';
const key = 'forms';

// eslint-disable-next-line react/prop-types
function companyPage(
  { dispatchLoadCompany,
    dispatchTrackCompany,
    company,
  }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  
  var requestURL = `${API_URL}${COMPANY_API}${SEARCH_PARAM}`;

  useEffect(() => {
    dispatchLoadCompany(requestURL);
  }, []);


  const handleOntrack = track => {
    dispatchTrackCompany(track)
    dispatchLoadCompany(requestURL);
  };

  var handleForwardBackwardSubmit = (ev) => {
    console.log("handleForwardBackwardSubmit",ev)
   dispatchLoadCompany(ev)
  }

  return (
    <div >
      <div>
      <CompanyBody
      data = {company.results}
      track = {handleOntrack}
      next_url = {company.next}
      prev_url = {company.previous}
      next_prev_handle = {handleForwardBackwardSubmit}
      />
      </div>
      
      <div>
        <Footer
        />
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  company: makeSelectCompany(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadCompany: requestURL => dispatch(fetchCompany(requestURL)),
  dispatchTrackCompany: track => dispatch(trackCompany(track)),

});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(companyPage);
