import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectDesign = state => state.forms || initialState;


export const makeSelectCompany = () =>
  createSelector(
    makeSelectDesign,
    forms => forms.company,
  );
