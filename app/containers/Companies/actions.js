import {
  FETCH_COMPANY,
  FETCH_COMPANY_SUCCESS,
  FETCH_COMPANY_FAILED,
  TRACK_COMPANY,
  TRACK_COMPANY_SUCCESS,
  TRACK_COMPANY_FAILED
} from './constants';

export function fetchCompany(requestURL) {
  return {
    type: FETCH_COMPANY,
    requestURL
  };
}

export function fetchCompanySearch(search) {
  return {
    type: FETCH_COMPANY,
    search,
  };
}


export function companyLoaded(payload) {
  return {
    type: FETCH_COMPANY_SUCCESS,
    payload,
  };
}
export function companyFailed(error) {
  return {
    type: FETCH_COMPANY_FAILED,
    error,
  };
}

export function trackCompany(track) {
  return {
    type: TRACK_COMPANY,
    track,
  };
}


export function companyTracked(payload) {
  return {
    type: TRACK_COMPANY_SUCCESS,
    payload
  };
}
export function companyTrackFailed(error) {
  return {
    type: TRACK_COMPANY_FAILED,
    error,
  };
}


