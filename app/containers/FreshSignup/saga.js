import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL,COMPANY_API,TRACK } from '../../assets/config';
import request from '../../utils/request';

import { 
  FETCH_COMPANY,
TRACK_COMPANY} from './constants';

import {
  companyLoaded,
  companyFailed,
  companyTrackFailed,
} from './actions';

import jwt_decode from 'jwt-decode'



export function* fetchCompanyData(data) {
  
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
  var requestURL = data.requestURL;
  
  // var search = location.pathname.split('=')[1]
  // if(search){
  //   requestURL = `${requestURL}${search}`;
  // }
  
  
  const companyGet = {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + access_token
        })
  }
  try {
    const companyResponse = yield call(request, requestURL,companyGet);
    console.log("DAta IS",companyResponse)
    /* Code for Question API call */
    yield put(companyLoaded(companyResponse))
    }
    catch (err) {
    console.log("ERROR",err)
    yield put(companyFailed(err));
  }
}

export function* trackCompany(track) {
  

  var userID = jwt_decode(JSON.parse(localStorage.getItem('authentication')).access).user_id
  var payload={
    user:userID,
    company:track.track.data.id
  }
  var access_token = JSON.parse(localStorage.getItem('authentication')).access
    const requestURL = `${API_URL}${TRACK}`;
    console.log("requestURL",requestURL);
  try {
    const trackResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + access_token
      }),
      body: JSON.stringify(payload),
    });
    console.log("trackResponse",trackResponse);
    // yield put(signup_Success(signupResponse));
    location.pathname = '/newuser';
  } catch (err) {
      console.log(err,err.response);
      yield put(companyTrackFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_COMPANY, fetchCompanyData);
  yield takeLatest(TRACK_COMPANY, trackCompany);
}
