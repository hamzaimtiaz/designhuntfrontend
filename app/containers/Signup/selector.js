import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectUser = state => state.signup || initialState;

export const makeSelectResults = () =>
  createSelector(
    makeSelectUser,
    signup => {
      return signup.authentication;
    },
  );

export const makeSelectSignUp = () =>
createSelector(
  makeSelectUser,
  globalState =>
      _.isEmpty(globalState.authentication) ? {} : globalState.authentication,
);
