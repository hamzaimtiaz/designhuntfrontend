/* eslint-disable no-console */
/* eslint-disable no-undef */
import { call, put, takeLatest } from 'redux-saga/effects';
import { SIGN_UP } from './constants';
import { API_URL,USER_API,TOKEN_API } from '../../assets/config';
import request from '../../utils/request';
import { signup_Success,signup_Failed } from './actions';

export function* signUpUser(signup_data) {
  const requestURL = `${API_URL}${USER_API}`;
    console.log("Data",JSON.stringify(signup_data));
  try {
    const signupResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(signup_data.payload),
    });
    console.log("SignupResponse",signupResponse);
    yield put(signup_Success(signupResponse));
    const requestURL1 = `${API_URL}${TOKEN_API}`;
      console.log("request URL",requestURL1)
      var login_data = {'username':signup_data.payload.username,'password':signup_data.payload.password}
      try {
        const tokenResponse = yield call(request, requestURL1, {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify(login_data),
        });

        yield put(signup_Success(tokenResponse));

        localStorage.setItem('authentication', JSON.stringify(tokenResponse));
        location.pathname='/newuser'
        
      } catch (err) {
        
        yield put(signup_Failed(err));
      }
    // location.pathname = '/newuser';
  } catch (err) {
      console.log(err,err.response);
      yield put(signup_Failed(err));
  }
}

export default function* signupSaga() {
  yield takeLatest(SIGN_UP, signUpUser);
}
