import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState,useRef } from 'react';
import { withRouter, useHistory  } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { signup } from './actions';
import { makeSelectResults,makeSelectSignUp } from './selector';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
const key = 'signup';

import '../css/components.css';
import '../css/design-hunt-d0b2cb.css';
import '../css/normalize.css';
import '../../assets/static/home.css';

function Signup({ dispatch_signup, signed_up,signup_error }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [signup_state, setsignup] = useState(
    {"email":""
    ,"password":""
    ,"first_name":""
    ,"last_name":""
    ,"username":"",}
    );

    const handleOnChange = ev => {
      if(ev.target.name == 'email'){
        signup_state.username=ev.target.value
        
      }
      setsignup({ ...signup_state, [ev.target.name]: ev.target.value });
  
    };

  const handleOnSubmit = () => {
    let user = "username"
    setsignup({ ...signup_state, [user]: signup_state['email']}); 

    console.log(signup_state)
    dispatch_signup(signup_state)
  };

  return (
    <div id="signup">
      <div className="section sign-up">
    <div>
      <div className="w-layout-grid grid-7">
        <div id="w-node-d8cad7c67299-ef2123d1" className="left-grid">
          <div className="div-block-39">
            <div className="div-block-40"><img src={require("../images/Designhunt-Logo.svg")} alt=""/></div>
            <div className="div-block-42">
              <h1 className="h3 white"><strong className="bold-text-8">Monitor competitors emails, facebook ads and website changes in one place. </strong></h1>
            </div>
            <div className="div-block-41"><img src={require("../images/Asset-1.svg")} alt="" className="image-12"/></div>
          </div>
        </div>
        <div className="right-grid">
          <div className="paragraph sign-in">Already a member? 
          <a href={`/login`}><span><strong className="bold-text-7">Sign in</strong></span></a>
          </div>
          <div className="div-block-43">
            <div>
              <h1 className="h2"><strong>Sign Up</strong></h1>
              <div className="paragraph">Lets create your account</div>
            </div>

            {signup_error.response &&
              <div>
                
              <div class="paragraph" ><p style={{ color: 'red' }}>Email  Already exists!</p></div>
            </div>
            }

            <div className="w-form">
              <form id="wf-form-Sign-Up-Form" name="wf-form-Sign-Up-Form" data-name="Sign Up Form" className="form" onSubmit={handleOnSubmit}>
                <div className="div-block-44">
                  <div className="div-block-45"><label for="first_name" className="field-label">First Name</label>
                  <input type="text" className="text-field w-input" maxlength="256" name="first_name" data-name="First Name" id="First-Name" required="" onChange={handleOnChange} value={signup_state.first_name}/></div>
                  <div className="div-block-46"><label for="last_name" className="field-label">Last Name</label>
                  <input type="text" className="text-field w-input" maxlength="256" name="last_name" data-name="Last Name" id="Last-Name" required="" onChange={handleOnChange} value={signup_state.last_name}/></div>
                </div>
                <div><label for="email" className="field-label">Email Address</label>
                <input type="email" maxlength="256" name="email" data-name="Email" id="Email" required="" className="text-field w-input" onChange={handleOnChange} value={signup_state.email}/>
                </div>
                <div><label for="password" className="field-label">Password</label>
                <input type="password" className="text-field w-input" maxlength="256" name="password" data-name="Password" id="Password" required="" onChange={handleOnChange} value={signup_state.password}/>
                </div>
                <input type="button" value="Submit" className="bluebutton sign-up w-button" style = {{cursor:'pointer'}} onClick={handleOnSubmit} /></form>
              
              <div className="w-form-done">
                <div>Thank you! Your submission has been received!</div>
              </div>
              <div className="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
              </div>
            </div>
            <p className="paragraph-5 centered">By joining, I agree to DesignHunt’s TOS and Privacy. Protected by reCAPTCHA and Google’s Privacy and Terms</p>
            {/* <div className="div-block-47"><img src="../images/or.svg" alt=""/></div> */}
            {/* <div className="w-layout-grid grid-9"><a href="../app/send-invitation.html" className="bluebutton google w-button"><strong>Sign up with Google</strong></a><a href="../app/send-invitation.html" className="bluebutton facebook w-button">Sign up with Facebook</a></div> */}
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  signed_up: makeSelectResults(),
  signup_error: makeSelectSignUp(),
});

const mapDispatchToProps = dispatch => ({
  dispatch_signup: signup_data => dispatch(signup(signup_data)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(Signup);
