import produce from 'immer';
// import { FETCH_USER_SUCCESS, LOGOUT } from './constants';
import { SIGN_UP_SUCCESS,SIGN_UP_FAILED } from './constants';

export const initialState = {
  authentication: {},
};

/* eslint-disable default-case, no-param-reassign */

const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      case SIGN_UP_SUCCESS:
        draft.authentication = action.payload;
        break;
      case SIGN_UP_FAILED:
        draft.authentication = action.error;
        break;

    }
  });

export default appReducer;
