/* eslint-disable prettier/prettier */
import { SIGN_UP, SIGN_UP_SUCCESS, SIGN_UP_FAILED } from './constants';

export function signup(payload) {
  // console.log('paload',payload);
  return {
    type: SIGN_UP,
    payload,
  };
} 

// eslint-disable-next-line camelcase
export function signup_Success(payload) {
  return {
    type: SIGN_UP_SUCCESS,
    payload,
  };
}
// eslint-disable-next-line camelcase
export function signup_Failed(error) {
  return {
    type: SIGN_UP_FAILED,
    error,
  };
}
