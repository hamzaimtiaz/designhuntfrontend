// export const API_URL = "http://127.0.0.1:8000/api/";
export const API_URL = "http://35.202.206.65:8000/api/";
export const USER_API = "users/";
export const TOKEN_API = "token/";
export const REFRESH_API = "refresh/";
export const EMAIL_API = "email/";
export const COMPANY_API = "company/";
export const SEARCH_PARAM = "?search=";
export const COMPANY_DETAIL_API = "company/";
export const NEW_SIGNUP_API = "newsignup/";
export const TRACK = "tracked/";
export const MAIN_API = "main/";
export const MAIN_API1 = "main1/";
export const FB_PAGE_API = "fb-page/";
export const PAGE_API = "page/";
export const FORM_API = "form/";
export const ANSWERED_API = "answered/";
export const ACCESS_TOKEN =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTg1ODM3OTA4LCJqdGkiOiI2MmY2YTBhMWEzMDM0YjRmYTUxMWNhZDU1YmEwMWNjYiIsInVzZXJfaWQiOjF9.y8fO_ZtrQRGVkVLrAzd4bDQMFOXOXdEQ5KycaC_qR_Q";
