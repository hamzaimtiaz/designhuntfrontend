import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';

var name = 'hamza'
function CompanyBody({
  data,
  track,
}) 
{
  var emails_jsx = <div></div>
  var fbpages_jsx = <div></div>
  var pages_jsx = <div></div>
  if (data.emails)
  {
    emails_jsx = data.emails.slice(0,4).map((data) =>    
    <div>
      
          <div class="updateitem"><a href={`/email-detail/${data.id}`} class="w-inline-block"><img src={data.screenshot} alt="" style={{'width': '3600px','height': '300px','object-fit': 'cover'}} class="image-23"/></a></div>
    </div>
  ); 
  }
  if (data.fbpages)
  {
    fbpages_jsx = data.fbpages.slice(0,4).map((data) =>    
    <div>
          <div class="updateitem"><a href={`/fb-detail/${data.id}`} class="w-inline-block"><img src={data.screenshot} alt="" style={{'width': '3600px','height': '300px','object-fit': 'cover'}} class="image-23"/></a></div>
    </div>
  ); 
  }
  if (data.pages)
  {
    pages_jsx = data.pages.slice(0,4).map((data) =>    
    <div>
          <div class="updateitem"><a href={`/page-detail/${data.id}`} class="w-inline-block"><img src={data.screenshot} alt="" style={{'width': '3600px','height': '300px','object-fit': 'cover'}} class="image-23"/></a></div>
    </div>
  ); 
  }

  return (
    <div>
  <div class="mobiledetail">
    <div class="div-block-147">
      <div class="w-layout-grid grid-33">
        <div class="div-block-146">
          <div><img src={data.image} alt="" class="image-22"/></div>
          <div class="div-block-148">
            <div class="h3"><strong>{data.name}</strong></div>
  <div class="text-block-19"><a href={data.website}>{data.website}</a></div>
          </div>
        </div>
        <div>
  <p class="paragraph">{data.summary}</p>
        </div>
        <div>
          <div class="div-block-101">
            <div><img src={require("../images/map-location.svg")} alt=""/></div>
  <div class="paragraph place">{data.address}</div>
          </div>
          <div class="div-block-102">
            <div class="div-block-101">
              <div><img src={require("../images/twitter_1.svg")} alt=""/></div>
  <div class="paragraph place">{data.twitter_followers} Followers</div>
            </div>
            <div class="div-block-101">
              <div><img src={require("../images/instagram_1.svg")} alt=""/></div>
  <div class="paragraph place">{data.instagram_followers} Followers</div>
            </div>
          </div>
        </div>
        <div>
        <div id="w-node-d485ace8c0b0-a05e595d" className="block__button"><a data-w-id="c3af5e1c-e91e-fc90-d845-d485ace8c0b1" href="#" className="blueline export w-button">Export</a><a href="#" className="green w-button">Track</a></div>
        {/* { data.is_tracked ?
              
              <input value="Tracked" type="submit" className="green w-button" style={{'background-color': '#e7e7e7', 'color': 'black'}}/>
              :<input value="Track" type="submit" className="green w-button" onClick={()=>track({data})}/>
           } */}
          
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper-block mobile-company-detail">
    <div class="section companylist">
      <div class="w-layout-grid gridcompanydetail">
        <div class="company-detail">
          <div class="div-block-100"><img src={data.image} alt="" class="image-24"/></div>
          <div class="detail">
            <div class="h3"><strong>{data.name}</strong></div>
            <div class="text-block-19"><a href={data.website}>{data.website}</a></div>
  <p class="paragraph">{data.summary}</p>
            <div class="div-block-101">
              <div><img src={require("../images/map-location.svg")} alt=""/></div>
  <div class="paragraph place">{data.address}</div>
            </div>
            <div class="div-block-102">
              <div class="div-block-101">
                <div><img src={require("../images/twitter_1.svg")} alt=""/></div>
  <div class="paragraph place">{data.twitter_followers} Followers</div>
              </div>
              <div class="div-block-101">
                <div><img src={require("../images/instagram_1.svg")} alt=""/></div>
                <div class="paragraph place">{data.instagram_followers} Followers</div>
              </div>
            </div>
          </div>
        </div>
        <div id="w-node-d485ace8c0b0-a05e595d" class="block__button">
          {/* <a data-w-id="c3af5e1c-e91e-fc90-d845-d485ace8c0b1" href="#" class="blueline export w-button">Export</a> */}
          { data.is_tracked ?
              
              <input value="Tracked" type="submit" className="green w-button" style={{'background-color': '#e7e7e7', 'color': 'black'}}/>
              :<input value="Track" type="submit" className="green w-button" onClick={()=>track({data})}/>
           }
            </div>
      </div>
    </div>
  </div>
  <div class="wrapper-block wide details">
    <div class="wrapper-block">
      <div class="updatewrapper email">
        <div class="div-block-103">
          <div class="h3 detailtitle"><strong>Latest emails</strong></div>
          {/* <div data-w-id="03177fbf-a6f6-2c81-6a43-2aa1c91cc2ce" class="view-more-link">View all 23 emails</div> */}
        </div>
        <div class="w-layout-grid grid__detailcompany">
        {emails_jsx}
        </div>
      </div>
      <div class="updatewrapper facebook-ads">
        <div class="div-block-103">
          <div class="h3 detailtitle"><strong>Latest Facebook Ads</strong></div>
          {/* <div data-w-id="7e6e1e77-78eb-10e1-1f6a-8ebe03026b83" class="view-more-link">View all 23 ads</div> */}
        </div>
        <div class="w-layout-grid grid__detailcompany">
        {fbpages_jsx}
        </div>
      </div>
      <div class="updatewrapper facebook-ads">
        <div class="div-block-103">
          <div class="h3 detailtitle"><strong>Latest Pages</strong></div>
          {/* <div data-w-id="c3c58c83-3a95-f05e-f2d8-86efad5ecce3" class="view-more-link">View all pages</div> */}
        </div>
        <div class="w-layout-grid grid__detailcompany">
        {pages_jsx}
        </div>
      </div>
    </div>
  </div>
  </div>
          
  );
}
export default CompanyBody;
