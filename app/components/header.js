import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';
import { DropdownButton,Dropdown } from 'react-bootstrap';

var name = 'hamza'
function Header({
  search,
  setSearch,
  onButtonSubmit
}) 
{
   
  var onSearchChange = ev =>{
    setSearch({...search,search:[ev.target.value]})
  }
  // href="../app/company-list.html"
  return (
    <div >
  <div class="section nav" >
    <div class="top-navigation"  >
      <div class="w-layout-grid grid-10" style={{ 'padding-left': '50px' }}>
        <div id="w-node-eadaba868b10-ba868b0d" class="div-block-50" >
          <a  class="w-inline-block">
          <a  href={`/home`} className="w-inline-block" >
            <img src={require("../images/Designhunt-Logo_1.svg")}  alt=""></img>
            </a>
            </a>
          <div class="div-block-51">
            {/* <form class="search-2 w-form" onSubmit={onButtonSubmit}> */}
            <div class="aParent" style={{ 'padding-left': '15px' }}>
              <div>
              {search &&
              
              <input type="search" class="search-input w-input" maxlength="256" name="query" placeholder="Search…" id="search" required="" value={search.search} onChange={onSearchChange}></input>}
              </div>
              <div style={{ 'font-size': '13.4px','margin-left': '0px' }}>
              <input value = "Submit" type="submit" name="search" class="search-button w-button" onClick={onButtonSubmit}></input>
              </div>
              
              </div>
              {/* </form> */}
          </div>
        </div>
        <div id="w-node-eadaba868b17-ba868b0d" class="div-block-52">
          <div data-delay="0" class="w-dropdown">
          {/* <div class="dropdown-toggle-3 w-dropdown-toggle"> */}
        
          <div className="dropdown">
            <div>
            <DropdownButton
              // alignRight
              title="More"
              id="dropdown-menu-align-right"
              style = {{ }}
              size="sm"
              
            >
              <Dropdown.Item eventKey="1" href="../app/setting-account.html" >Account</Dropdown.Item>
              <Dropdown.Item eventKey="2" href="../app/setting-account.html">Plan and Billing</Dropdown.Item>
              {/* <Dropdown.Item eventKey="5" href={'/home'}>Home</Dropdown.Item> */}
              <Dropdown.Item eventKey="3" href={'/company'}>Track Companies</Dropdown.Item>
              <Dropdown.Item eventKey="4" href={'/login'} >Logout</Dropdown.Item>
            </DropdownButton>
          
            


            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  );
}
export default Header;
