import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';
var name = 'hamza'
function Footer({
}) 
{
  return (
    <div>
  <div>
    <div class="div-block-26" style={{ 'padding-left': '10px' }}>
      <div class="div-block-151">
        <div data-collapse="none" data-animation="default" data-duration="400" class="footer-navbar w-nav">
          <div class="footer wrap w-container"><a href="../index.html" class="brand-3 w-nav-brand"><img src={require("../images/Designhunt-Logo_1.svg")}  alt=""/></a>
            <nav role="navigation" class="w-nav-menu">
              <div class="w-layout-grid grid-6">
                <div>
                  <div class="text-block-3"><strong class="bold-text-4">Company</strong></div>
                  <div class="text-block-4"><a href="../about-us.html" class="footerlink">About Us</a></div>
                  <div class="text-block-4">Careers</div>
                  <div class="text-block-4"><a href="../press.html" class="footerlink">Press</a></div>
                </div>
                <div class="div-block-150">
                  <div class="text-block-3"><strong class="bold-text-5">Product</strong></div>
                  <div class="text-block-4">How it Works</div>
                  <div class="text-block-4">Why Us</div>
                  <div class="text-block-4">Pricing</div>
                  <div class="text-block-4">Portfolios</div>
                </div>
                <div>
                  <div class="text-block-3"><strong class="bold-text-6">Resources</strong></div>
                  <div class="text-block-4"><a href="../blog.html" class="footerlink">Blog</a></div>
                  <div class="text-block-4"><a href="../faqs.html" class="footerlink">FAQs</a></div>
                  <div class="text-block-4"><a href="../testimonials.html" class="footerlink">Testimonials</a></div>
                </div>
                <div id="w-node-14e274b93286-74b9325e" class="div-block-30">
                  <div class="div-block-31"><img src={require("../images/twitter.svg")} alt=""/></div>
                  <div class="div-block-32"><img src={require("../images/instagram.svg")} alt=""/></div>
                </div>
              </div>
            </nav>
            <div class="w-nav-button">
              <div class="w-icon-nav-menu"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="div-block-138">
        <div class="text-block-27"><a href="../privacy.html" class="link-7">Privacy Policy</a></div>
        <div class="text-block-27"><a href="../terms.html" class="link-7">Term of Service</a></div>
      </div>
    </div>
    </div>
    </div>
  );
}
export default Footer;
