import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';

var name = 'hamza'

function EmailBody({
  data,
  track,
}) 
{
  const date_converter = data => {
      return new Date(data).toISOString().slice(0, 10)
  }

  return (
    <div>
      {data.company &&
  <div class="wrapper-block wide email">
    <div>
      <div class="w-layout-grid grid-21">
        <div id="w-node-c79f9cd3888e-6f79ac20" class="div-block-104">
          <div class="div-block-120">
            <div class="div-block-121">
              <a href="#" class="tag blue w-button">Email</a>
              <a href="#" class="tag blue w-button">Desktop</a>
              </div>
              </div>
          <div class="div-block-122">
            <img src={data.screenshot} alt="" class="image-25"></img>
            
            </div>
        </div>
        <div class="div-block-105">
          <div class="div-block-119">
            <div class="w-layout-grid gridemail">
              <div class="company-detail">
                <div class="detail">
                  <div class="w-layout-grid grid-22">
                    <div id="w-node-c79f9cd3889d-6f79ac20" class="div-block-108">
                      
                      <div class="h3"><a href={`/company-detail/${data.company.id}`} className="link-11"><strong>{data.company.name}</strong></a></div>
                      
                      <div class="text-block-19"><a href={data.company.website}>{data.company.website}</a></div>
                    </div>
                    <div class="div-block-107"><img src={data.company.image} alt="" class="image-16"></img></div>
                  </div>
                  <p class="paragraph">{data.company.summary}</p>
                  <div class="div-block-111">
                    <div><img src={require("../images/map-location.svg")} alt=""></img></div>
                    <div class="paragraph place">{data.company.address}</div>
                  </div>
                  <div class="div-block-110">
                    <div class="div-block-109 twitter">
                      <div><img src={require("../images/twitter.svg")} alt=""></img></div>
                      <div class="paragraph place" style={{'font-size': '13px'}}>{data.company.twitter_followers} Followers</div>
                    </div>
                    <div class="div-block-101">
                      <div><img src={require("../images/instagram_1.svg")} alt=""></img></div>
                      <div class="paragraph place" style={{'font-size': '13px'}}>{data.company.instagram_followers} Followers</div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="w-node-c79f9cd388b8-6f79ac20" class="block__button">
              { data.company.is_tracked ?
              
                      <input value="Tracked" type="submit" className="green w-button" style={{'background-color': '#e7e7e7', 'color': 'black'}}/>
                      :<input value="Track" type="submit" className="green w-button" onClick={()=>track({data})}/>
                   }
                    </div>
             
            </div>
            <div class="div-block-112"></div>
            <div>
              <div class="h5"><strong>Details</strong></div>
              <div class="w-layout-grid grid-23">
                <div>
                  <div class="text-block-21"><strong class="bold-text-12">Email date<br/>Number of emails tracked</strong></div>
                </div>
                <div >
                  <div class="text-block-22" ><strong class="bold-text-12" style={{'white-space': 'nowrap'}}>{date_converter(data.creation_time) }<br/>{data.emails_tracked}</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
}
  </div>
          
  );
}
export default EmailBody;
