// import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import ExampleComponent from "react-rounded-image";
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';
import moment from 'moment'
import { string } from 'prop-types';


function MainBody({
  data,
  next_url,
  prev_url,
  latest_data,
  next_prev_handle,
  handleSubmit,
  tabState,
  untrack,
  company_count,
}) 
{
  var emails_jsx =<div></div>;
  var fb_page_jsx=<div></div>;
  var page_jsx=<div></div>;
  var latestupdate_jsx = <div></div>;
  var company_tracked_jsx = <div></div>;
  var company_images = <div></div>;

  var forward_backward_email_jsx = <div></div>
  var forward_backward_fbpage_jsx = <div></div>
  var forward_backward_page_jsx = <div></div>
  var forward_backward_track_jsx = <div></div>

  var latest_arr = []
  var latest_arr_sorted = []

  if(data)
  {
    // Forward Backward for email
    if(next_url && prev_url && data.Email.length == 12)
    {

      forward_backward_email_jsx = 
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }

    else if (next_url && data.Email.length == 12)
    {
      forward_backward_email_jsx =
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }
    else if (prev_url && next_url && data.Email.length < 12)
    {
      forward_backward_email_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
    else if (prev_url && data.Email.length < 12)
    {
      forward_backward_email_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

    else if (prev_url )
    {
      forward_backward_email_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

    // For FBPage

    if(next_url && prev_url && data.FBPage.length == 12)
    {

      forward_backward_fbpage_jsx = 
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }

    else if (next_url && data.FBPage.length == 12)
    {
      forward_backward_fbpage_jsx =
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }
    else if (prev_url && next_url && data.FBPage.length < 12)
    {
      forward_backward_fbpage_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
    else if (prev_url && data.FBPage.length < 12)
    {
      forward_backward_fbpage_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

    else if (prev_url )
    {
      forward_backward_fbpage_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
     // For Page

     if(next_url && prev_url && data.Page.length == 12)
    {

      forward_backward_page_jsx = 
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }

    else if (next_url && data.Page.length == 12)
    {
      forward_backward_page_jsx =
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }
    else if (prev_url && next_url && data.Page.length < 12)
    {
      forward_backward_page_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
    else if (prev_url && data.Page.length < 12)
    {
      forward_backward_page_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

    else if (prev_url )
    {
      forward_backward_page_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

     // For Track

     if(next_url && prev_url && data.Track.length == 12)
    {

      forward_backward_track_jsx = 
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }

    else if (next_url && data.Track.length == 12)
    {
      forward_backward_track_jsx =
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
    }
    else if (prev_url && next_url && data.Track.length < 12)
    {
      forward_backward_track_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
    else if (prev_url && data.Track.length < 12)
    {
      forward_backward_track_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }

    else if (prev_url )
    {
      forward_backward_track_jsx =
      <div>
        <a href="#" class="next round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
    }
 

  }

  if(latest_data)
  {
    for (const entry in latest_data.Email) {
      latest_arr.push({
        key:   new Date(latest_data.Email[entry].creation_time),
        value: latest_data.Email[entry]
    });
    }
    for (const entry in latest_data.FBPage) {
      latest_arr.push({
        key:   new Date(latest_data.FBPage[entry].creation_time),
        value: latest_data.FBPage[entry]
    })
    }
    for (const entry in latest_data.Page) {
      latest_arr.push({
        key:   new Date(latest_data.Page[entry].creation_time),
        value: latest_data.Page[entry]
    })
    }
    var keys = Object.keys(latest_arr);
    keys.sort();

    // console.log("Keys",keys)
    var latest_arr_asc = {};

    var count =0 
    for (var i=keys.length; i>0; i--) { // now lets iterate in sort order
      
      var key = keys[i-1];
      var value = latest_arr[key];
      if(count <12){
        latest_arr_sorted.push(value)
      }
      
      count = count+1
      /* do something with key & value here */
    }
    
  }

  if(data){
    // console.log("Data Email is",data.Email)
  if (data.Email){
    emails_jsx = data.Email.map(email =>
    <div>  
        
  <div className="updateitem emails" key={email.id}>
                  <div className="w-layout-grid tittle emails">
                    <div className="div-block-98">
                      <div className="div-block-97"><a href={`/company-detail/${email.company.id}`} className="w-inline-block" >
                      <ExampleComponent image={email.company.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent> 
                        {/* <img src={email.company.image} alt=""/> */}
                        </a></div>
                      <div className="div-block-113" style={{margin: "15px"}}>
                      <h1 className="h5 companies-tracked">
                      <a href={`/company-detail/${email.company.id}`} className="link-11">{email.company.name}</a>
                      </h1>
                        <p className="paragraph company-desc">{email.company.caption}</p>
                      </div>
                    </div>
                  </div>
                  <a href={`/email-detail/${email.id}`}><img src={email.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a>
                  </div>

                  </div>
  ); 
  }
  }

  if(data){
  if (data.FBPage)
  {
    fb_page_jsx = data.FBPage.map((fb_page) =>    
    <div className="updateitem">
    <div className="w-layout-grid tittle">
      <div className="div-block-98">
        <div className="div-block-97">
          <a href={`/company-detail/${fb_page.company.id}`} className="w-inline-block" >
            <ExampleComponent image={fb_page.company.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent> 
            {/* <img src={fb_page.company.image} alt=""/> */}
            </a></div>
        <div className="div-block-113" style={{margin: "15px"}}>
          <h1 className="h5 companies-tracked">
            <a href={`/company-detail/${fb_page.company.id}`} className="link-11">{fb_page.company.name}</a></h1>
          <p className="paragraph company-desc">{fb_page.company.caption}</p>
        </div>
      </div>
    </div><a href={`/fb-detail/${fb_page.id}`}>
      <img src={fb_page.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a></div>
  ); 
  }
}

if(data){
  if (data.Page)
  {
    page_jsx = data.Page.map((page) =>    
    <div className="updateitem">
                  <div className="w-layout-grid tittle">
                    <div className="div-block-98">
                      <div className="div-block-97">
                      <a href={`/company-detail/${page.company.id}`} className="w-inline-block" >
                      <ExampleComponent image={page.company.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent> 
                        {/* <img src= alt=""/> */}
            </a></div>
                      <div className="div-block-113" style={{margin: "15px"}}>
                        <h1 className="h5 companies-tracked">
                        <a href={`/company-detail/${page.company.id}`} className="link-11">{page.company.name}</a></h1>
                        
                        <p className="paragraph company-desc">{page.company.caption}</p>
                      </div>
                    </div>
                  </div>
                  <a href={`/page-detail/${page.id}`}>
                    <img src={page.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a>
                  </div>
  ); 
  }
}

  if (latest_arr_sorted){
    latestupdate_jsx = latest_arr_sorted.map(latest =>
    <div>  
        {latest.value.company &&
        <div className="updateitem">
          
                  <div className="w-layout-grid tittle">
                    <div className="div-block-98">
                      <div className="div-block-97">
                        <a  href={`/company-detail/${latest.value.company.id}`} className="w-inline-block" >
                        <ExampleComponent image={latest.value.company.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent>   
                        
            </a>
            </div>
                      <div className="div-block-113" style={{margin: "15px"}}>
                       
                      <a href={`/company-detail/${latest.value.company.id}`} className="h5 companies-tracked" >
                        {latest.value.company.name}</a>
                      <p className="paragraph company-desc" >{latest.value.company.caption}</p>
                      </div>
                    </div>
                    <div id="w-node-02b9a59dc2a8-773164f6" style={{color: "grey"}}><a  className="tag w-button">{latest.value.type}</a></div>
                  </div>

                  {latest.value.type === "Email" && 
                  <div>
                  <a href={`/email-detail/${latest.value.id}`}>
                  <img src={latest.value.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a>
                  </div>
                  }
                  {latest.value.type === "FbPage" && 
                  <div>
                  <a href={`/fb-detail/${latest.value.id}`}>
                  <img src={latest.value.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a>
                  </div>
                  }
                  {latest.value.type === "Page" && 
                  <div>
                  <a href={`/page-detail/${latest.value.id}`}>
                  <img src={latest.value.screenshot} alt="" style={{'width': '600px','height': '300px','object-fit': 'cover'}}/></a>
                  </div>
                  }
                  
                    </div>
                    
  }
                  </div>
  ); 
  }
  var tracked_company_number = 
  <div>          
  <div className="company__description">
  <div className="h3"><strong>Tracked Companies</strong></div>
  <div className="paragraph">
  {company_count} Companies tracked. </div>
  </div>
  </div>

     if(data)
     {       
  if (data.Track){
    company_tracked_jsx = data.Track.map(tracked =>
    <div> 
      <div className="div-block-96">
                  <div className="w-layout-grid grid-19">
                    <div className="div-block-98">
                      <div className="div-block-97">
                      <a  href={`/company-detail/${tracked.company.id}`} className="w-inline-block" >
                      <ExampleComponent image={tracked.company.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent> 
                       </a>
                        </div>
                      <div className="div-block-99">
                        <h1 className="h5 companies-tracked">{tracked.company.name}</h1>
                        <p className="paragraph companies-tracked">{tracked.company.caption}</p>
                      </div>
                    </div>
                    <div id="w-node-a10f338d66d7-773164f6">
                      <input value="Untrack" type="submit" class="redbutton w-button" onClick={()=>untrack({tracked})}/>
                      
                      </div>
                  </div>
                  <div className="w-layout-grid grid-20">
                    <div id="w-node-58a9a8fa1a6f-773164f6"><img src={tracked.company.image} alt=""/></div>
                    <div id="w-node-aa960d674470-773164f6"><img src={tracked.company.image} alt=""/></div>
                    <div id="w-node-48719eaeb468-773164f6"><img src={tracked.company.image} alt=""/></div>
                  </div> 
                  </div>
    </div>
  );
  
  company_images=data.Track.map(tracked =>
    <div className="company__icon overlap">
      {/* <div ><img src={tracked.company.image} alt="" className="company__icon-logo"/></div> */}
    </div>
      
   
    );
   
  
  
  }
}

  var pro_version = <div className="upgrade-to-pro-container">
  <div className="upgrade">
    <div className="div-block-114">
      <h1 className="h2 upgradetopro"><strong>Upgrade to Pro</strong></h1>
      <div className="text-block-23">Upgrade for unlimited access to all 10,000+ design that are available</div>
    </div>
    <div className="w-layout-grid grid-24">
      <div id="w-node-b509de281f41-de281f39" className="div-block-159"><img src={require("../images/price-illustration.svg")} alt="" className="image-17"/></div>
      <div id="w-node-b509de281f43-de281f39">
        <p className="paragraph upgradetopro">You’re limited to data starting Jan 13 — all Pro plans unlock all historical data. What’s more — you’ll receive access to powerfull User Journeys, detailed Reports and you can track even more brands.<br/>‍<br/>By upgrading your plan you will unlock unlimited historical data immediately.</p>
      </div>
    </div>
    <div className="div-block-115">
      <div className="w-layout-grid grid-25">
        <div id="w-node-b509de281f4c-de281f39" className="div-block-117">
          <div className="div-block-116">
            <div className="h5 center"><strong>For $230 a year,  </strong></div>
            <p className="paragraph center upgradeplan">You can access to all 1,838 emails that are available. Have a question about the pricing? <strong className="bold-text-13">Contact our sales here.</strong></p>
          </div>
        </div>
        <div id="w-node-b509de281f57-de281f39" className="div-block-118"><a className="bluebutton upgradeplan w-button">Upgrade Plan</a></div>
      </div>
    </div>
  </div>
</div>;
   
  return (
    
<div>
    <div className="wrapper-block company__list">
    <div className="section companylist">
      <div className="w-layout-grid grid__companylist">
        <div className="company">
         {tracked_company_number}
        </div>
        <div id="w-node-5499ab189ef2-773164f6" className="block__button"><a className="bluebutton tracked-companies w-button">Upgrade Plan</a></div>
      </div>
    </div>
  </div>
  <div className="wrapper-block wide" style={{ padding: '15px' }}>
    <div className="wrapper-block">
      <div >
        <div data-duration-in="300" data-duration-out="100" className="tabs__company w-tabs">
          <div className="tabs-menu-3 w-tab-menu">
            <a data-w-tab="Tab 1" style={{ color: 'grey' }} className={tabState.header_arr[0][0]} onClick={()=>handleSubmit(0)}>
              <div >Latest Update</div>
            </a>
            <a data-w-tab="Tab 2" style={{ color: 'grey' }} className={tabState.header_arr[0][1]} onClick={()=>handleSubmit(1)}>
              <div>Emails</div>
            </a>
            <a data-w-tab="Tab 3" style={{ color: 'grey' }} className={tabState.header_arr[0][2]} onClick={()=>handleSubmit(2)}>
              <div>Facebook Ads</div>
            </a>
            <a data-w-tab="Tab 4" style={{ color: 'grey' }} className={tabState.header_arr[0][3]} onClick={()=>handleSubmit(3)}>
              <div>Pages</div>
            </a>
            <a data-w-tab="Tab 5"  style={{ color: 'grey' }} className={tabState.header_arr[0][4]} onClick={()=>handleSubmit(4)}>
              <div>Companies</div>
            </a>
          </div>

          <div className="tabs-content-4 w-tab-content">
            <div data-w-tab="Tab 1" className={tabState.inner_arr[0][0]}>
              <div className="latestupdate__text">
                <div className="h4"><strong>Latest Update</strong></div>
                <p className="paragraph latest-update">View recent emails, facebook ads, and pages from your tracked companies.</p>
              </div>
              <div className="w-layout-grid gridthumbnail latestupdate">
                {latestupdate_jsx}
                      
              </div>
              {pro_version}
              </div>

            <div data-w-tab="Tab 2" className={tabState.inner_arr[0][1]}>
              <div className="latestupdate__text">
                <div className="h4"><strong>Email</strong></div>
                <p className="paragraph latest-update">View recent emails from your tracked companies.</p>
              </div>
              <div className="w-layout-grid gridthumbnail emails">
              {emails_jsx}
              
              </div>
              <div>
              {forward_backward_email_jsx}
              </div>
              {pro_version}
            </div>

            <div data-w-tab="Tab 3" className={tabState.inner_arr[0][2]}>
              <div className="latestupdate__text">
                <div className="h4"><strong>Facebook Ads</strong></div>
                <p className="paragraph latest-update">View recent Facebook ads from your tracked companies.</p>
              </div>
              <div className="w-layout-grid gridthumbnail latestupdate">
                
                {fb_page_jsx}

              </div>
              <div>
              {forward_backward_fbpage_jsx}
              </div>
              {pro_version}
            </div>

            <div data-w-tab="Tab 4" className={tabState.inner_arr[0][3]}>
              <div className="latestupdate__text">
                <div className="h4"><strong>Pages</strong></div>
                <p className="paragraph latest-update">View recent pages from your tracked companies.</p>
              </div>
              <div className="w-layout-grid gridthumbnail latestupdate">
                {page_jsx}             
              </div>

              <div>
              {forward_backward_page_jsx}
              </div>

              {pro_version}
            </div>

            <div data-w-tab="Tab 5" className={tabState.inner_arr[0][4]}>
              <div className="latestupdate__text">
                <div className="h4"><strong>Companies Tracked</strong></div>
              </div>
              <div className="w-layout-grid grid__companytracked">
              {company_tracked_jsx}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
        
  );
}
export default MainBody;
