import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, Fragment } from 'react';
import { Link } from "react-router-dom";
import ExampleComponent from "react-rounded-image";
import './css/components.css';
import './css/design-hunt-d0b2cb.css';
import './css/normalize.css';
import '../assets/static/home.css';
import GoogleFontLoader from 'react-google-font-loader';
import {WebFont,} from 'webfontloader';



function CompanyBody({
  data,
  next_url,
  prev_url,
  next_prev_handle,
  track,
}) 
{
  var not_tracked_arr = []
 var empty = "  "
  // if(data)
  // {
  //   for (const entry in data) {
      
  //     if(!data[entry].is_tracked)
  //     not_tracked_arr.push(data[entry]);
  
  //   }  
  // }
  
  
  var company_jsx = <div></div>;
  var next_prev_jsx = <div></div>

  if (next_url && prev_url)
  {
   next_prev_jsx = 
      <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
  }

  else if (next_url)
  {
   next_prev_jsx = 
      <div>
      <a href="#" class="next round" onClick={()=>next_prev_handle(next_url)}>&#8250;</a>
      </div>
  }
  else if (prev_url)
  {
    next_prev_jsx =
    <div>
        <a href="#" class="previous round" onClick={()=>next_prev_handle(prev_url)}>&#8249;</a>
      </div>
  }

  var search_company_number = <div className="text-block-33">0 Search Result</div>

  if (data){
      company_jsx = data.map(data =>
       
        <div>
          <div className="div-block-96">
     
                  <div className="w-layout-grid grid-19">
                    <div className="div-block-98">
                      <div className="div-block-97">
                      <a  href={`/company-detail/${data.id}`} className="w-inline-block" >
                      <ExampleComponent image={data.image}
                        roundedSize="0"
                        imageWidth="70"
                        imageHeight="70"></ExampleComponent> 
                        </a>
                        </div>
                      <div className="div-block-99">
                        <h1 className="h5 companies-tracked">{data.name}</h1>
                        <p className="paragraph companies-tracked">{data.caption}</p>
                      </div>
                    </div>
                    
                    <div id="w-node-a10f338d66d7-773164f6">
                    { data.is_tracked ?
                      <input value="Tracked" type="submit" className="button w-button" style={{'background-color': '#e7e7e7', 'color': 'black'}}/>
                      :<input value="Track" type="submit" className="button w-button" onClick={()=>track({data})}/>
                    }
                    
                      </div>
                  </div>
                  <div className="w-layout-grid grid-20">
                    <div id="w-node-58a9a8fa1a6f-773164f6"><img src={data.image} alt=""/></div>
                    <div id="w-node-aa960d674470-773164f6"><img src={data.image} alt=""/></div>
                    <div id="w-node-48719eaeb468-773164f6"><img src={data.image} alt=""/></div>
                  </div> 
                  </div>
                    
  </div>

    ); 
    search_company_number = 
    <div className="text-block-33">{data.length} Search Result</div>
    }
 
  return (
    
  <div >
<div >
 
        <div className="h3" style={{padding: '20px 20px'}}><strong>Track Companies</strong></div>
  
  <div className="wrapper-block wide" style={{ padding: '15px' }}>
    <div className="empty-dashboard-company-list">
      <div className="text-block-35">Search to track a brand</div>
      <div className="empty-dashboard-search-form">
        <div className="w-form">
       
        </div>
      </div>
    </div>


    <div className="w-layout-grid company-list">
      {company_jsx}
     </div>
     <div>
       {next_prev_jsx}
     </div>
  </div>
  </div>
  </div>
        
          
  );
}
export default CompanyBody;
